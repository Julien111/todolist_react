import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Todo from './components/Todo'
import "./css/index.css";

function App() {
  return (
    <div className="container">
      <Todo />
    </div>
  );
}

export default App;
