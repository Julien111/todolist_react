import React, { Component } from "react";

class Todo extends Component {
  state = {
    element: "",
    items: [],
  };

  //Ici on regarde qu'elle est le name et on va récupérer la value (event.target.value)

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });

    //console.log(this.state.element);
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.setState({
      element: "", //on remet élément à zéro
      items: [...this.state.items, { element: this.state.element }],
    });
  };

  deleteItem = (index) => {
      const tab = this.state.items;
      tab.splice(index, 1);
      this.setState({ 
          item : tab,
      })
  }

  //Fonction qui retourne nos elements

  renderTodo = () => {
    return this.state.items.map((item, index) => {
      return (
        <div className="card mb-3 carteRep" key={index}>
          <div className="card-body">
            <h4>
              <span>{item.element} </span>

              {/* On utilise une fonction anonyme avant deleteItem pour passer un paramètre */}

              <i
                className="fas fa-times"
                onClick={() => this.deleteItem(index)}
                style={{ cursor: "pointer", float: "right", color: "red" }}
              ></i>
            </h4>
          </div>
        </div>
      );
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="card my-3">
          <div className="card-header">To-Do List</div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label className="sousTitre" htmlFor="element">Chose à faire :</label>
                <input
                  type="text"
                  className="form-control form-control-lg"
                  name="element"
                  onChange={this.onChange}
                  value={this.state.element}
                />
              </div>
              <button className="btn btn-primary btn-block">
                Ajouter une tâche
              </button>
            </form>
          </div>
        </div>

        {this.renderTodo()}
      </React.Fragment>
    );
  }
}

export default Todo;
